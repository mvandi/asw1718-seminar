import { Component, OnInit, Input } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Todo, clone } from '../todo';
import { TodoService } from '../todo.service';
import { EditTodoComponent } from '../edit-todo/edit-todo.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input()
  todo: Todo;

  constructor(private service: TodoService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.service.onUpdated()
      .filter(todo => todo._id === this.todo._id)
      .subscribe(todo => {
        this.todo = todo;
      });
  }

  edit() {
    const editTodo = this.modalService.open(EditTodoComponent).componentInstance;
    editTodo.todo = clone(this.todo);
    editTodo.oldTitle = this.todo.title;
  }

  delete() {
    this.service.delete(this.todo);
  }

}
