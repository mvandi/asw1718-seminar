import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { EditTodoComponent } from './edit-todo/edit-todo.component';

import { TodoService } from './todo.service';
import { EventBusService } from './eventbus.service';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    TodoComponent,
    EditTodoComponent
  ],
  providers: [TodoService, EventBusService],
  entryComponents: [EditTodoComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
