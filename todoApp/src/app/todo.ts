export interface Todo {
    _id?: string;
    title: string;
    done: boolean;
}

export function clone(todo: Todo): Todo {
    return {
        _id: todo._id,
        title: todo.title,
        done: todo.done
    };
}
