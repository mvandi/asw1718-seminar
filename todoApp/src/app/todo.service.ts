import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';

import { Todo } from './todo';
import { EventBusService } from './eventbus.service';

@Injectable()
export class TodoService {

  private static TODOS = '/api/todos';
  private static EVENTS = TodoService.TODOS + '/events';

  private stored: Subject<Todo>;
  private updated: Subject<Todo>;
  private destroyed: Subject<string>;

  constructor(private http: HttpClient, eventBus: EventBusService) {
    this.stored = new Subject<Todo>();
    this.updated = new Subject<Todo>();
    this.destroyed = new Subject<string>();

    eventBus.connect(TodoService.EVENTS);
    eventBus.registerHandler('todos.stored', (err, msg) => {
      this.stored.next(msg.body);
    });

    eventBus.registerHandler('todos.updated', (err, msg) => {
      this.updated.next(msg.body);
    });

    eventBus.registerHandler('todos.destroyed', (err, msg) => {
      this.destroyed.next(msg.body._id);
    });
  }

  query(): Observable<Todo[]> {
    return this.http.get<Todo[]>(TodoService.TODOS);
  }

  save(todo: Todo) {
    this.http.post<Todo>(TodoService.TODOS, todo).subscribe();
  }

  update(todo: Todo) {
    this.http.put<Todo>(TodoService.TODOS + '/' + todo._id, todo).subscribe();
  }

  delete(todo: Todo) {
    this.http.delete<Todo>(TodoService.TODOS + '/' + todo._id).subscribe();
  }

  onStored(): Observable<Todo> {
    return this.stored.asObservable();
  }

  onUpdated(): Observable<Todo> {
    return this.updated.asObservable();
  }

  onDestroyed(): Observable<string> {
    return this.destroyed.asObservable();
  }

}
