import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { Todo } from '../todo';

import { TodoService } from '../todo.service';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.css']
})
export class EditTodoComponent implements OnInit {

  @Input()
  todo: Todo;

  @Input()
  oldTitle: string;

  private _error = new Subject<void>();
  error = false;

  constructor(private service: TodoService, public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    this._error.pipe(
      debounceTime(5000)
    ).subscribe(() => this.error = false);
  }

  save() {
    if (this.todo.title.length < 1) {
      this.error = true;
      this._error.next();
      return;
    }
    this.service.update(this.todo);
    this.activeModal.close();
  }

}
