import { Component, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { remove } from 'lodash';

import { Todo } from './todo';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  todos: Todo[] = [];

  title = '';

  private _error = new Subject<void>();
  error = false;

  constructor(private service: TodoService) {
  }

  ngOnInit() {
    this.service.query()
      .subscribe(todos => this.todos = todos);

    this.service.onStored()
      .subscribe(todo => {
        this.todos.push(todo);
      });

    this.service.onDestroyed()
      .subscribe(oid => {
        remove(this.todos, todo => todo._id === oid);
      });

    this._error.pipe(
      debounceTime(5000)
    ).subscribe(() => this.error = false);
  }

  onKeyPressed(e) {
    if (e.keyCode === 13) {
      this.save();
    }
  }

  save() {
    if (this.title.length < 1) {
      this.error = true;
      this._error.next();
      return;
    }
    this.service.save({
      title: this.title,
      done: false
    });
    this.title = '';
  }

}
