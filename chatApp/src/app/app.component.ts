import { Component, OnInit } from '@angular/core';
import { Message } from './message';
import { MessageService } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  messages: Message[] = [];
  content = '';

  constructor(private service: MessageService) {
  }

  ngOnInit() {
    this.service.onMessage()
      .subscribe(message => this.messages.push(message));
    this.service.onMessages()
      .subscribe(messages => this.messages = messages);
  }

  onKeyPressed(e) {
    if (e.keyCode === 13) {
      this.send();
    }
  }

  send() {
    if (!this.content || this.content.length < 1) {
      console.warn('Warning: content is ' + this.content);
      return;
    }
    this.service.send({
      content: this.content,
      timestamp: new Date()
    });
    this.content = '';
  }

}
