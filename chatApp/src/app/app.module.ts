import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MessageComponent } from './message/message.component';

import { MessageService } from './message.service';
import { EventBusService } from './eventbus.service';

@NgModule({
  declarations: [
    AppComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [MessageService, EventBusService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
