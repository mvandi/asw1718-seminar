export interface Message {
    content: string;
    timestamp: Date;
}

export function clone(message: Message) {
    return {
        content: message.content,
        timestamp: message.timestamp
    }
}
