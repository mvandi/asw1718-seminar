import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { EventBusService } from './eventbus.service';
import { Message } from './message';

@Injectable()
export class MessageService {

  private static EVENTS = '/api/messages/events';

  private message: Subject<Message>;
  private messages: Subject<Message[]>;

  constructor(private eventBus: EventBusService) {
    this.message = new Subject<Message>();
    this.messages = new Subject<Message[]>();

    eventBus.connect(MessageService.EVENTS);
    eventBus.registerHandler('chat.to.client.messages', (err, msg) => {
      if (err) {
        console.error(err);
        return;
      }
      console.log('Received messages');
      this.messages.next(msg.body);
      this.messages.complete();
    });

    eventBus.registerHandler('chat.to.client.message', (err, msg) => {
      if (err) {
        console.error(err);
        return;
      }
      this.message.next(msg.body);
    });
  }

  send(message: Message) {
    this.eventBus.send('chat.to.server.message', {
      content: message.content,
      timestamp: message.timestamp.toUTCString()
    });
  }

  onMessage(): Observable<Message> {
    return this.message.asObservable();
  }

  onMessages(): Observable<Message[]> {
    setTimeout(() => {
      this.eventBus.send('chat.to.server.messages', {
        a: true
      });
    });
    return this.messages.asObservable();
  }

}
