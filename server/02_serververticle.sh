#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "$0 <http_port>" >&2 
    exit 1
fi
java -cp build/libs/server-1.0.jar it.unibo.asw.vertx_samples.ex02.ServerVerticle $1
