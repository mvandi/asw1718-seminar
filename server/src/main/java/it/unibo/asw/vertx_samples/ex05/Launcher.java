package it.unibo.asw.vertx_samples.ex05;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static it.unibo.asw.vertx_samples.util.LauncherUtils.getPort;

public class Launcher {

    public static void main(String[] args) throws Exception {
        final int port = getPort(args);
        final JsonObject config = config("config.json").put("httpPort", port);

        final DeploymentOptions options = new DeploymentOptions().setConfig(config);

        Vertx.clusteredVertx(new VertxOptions(), ar -> {
            if (ar.succeeded()) {
                final Vertx vertx = ar.result();
                vertx.deployVerticle(RESTfulApi::new, options);
            } else {
                System.err.println("Could not start RESTfulApi verticle: " + ar.cause().getMessage());
            }
        });
    }

    private static JsonObject config(final String path) throws IOException {
        return new JsonObject(Files.readAllLines(Paths.get(path)).stream()
                .collect(Collectors.joining())
                .trim());
    }

}
