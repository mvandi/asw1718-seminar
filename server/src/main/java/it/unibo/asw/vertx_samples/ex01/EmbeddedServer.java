package it.unibo.asw.vertx_samples.ex01;

import io.vertx.core.Vertx;

import static it.unibo.asw.vertx_samples.util.LauncherUtils.getHostAddress;
import static it.unibo.asw.vertx_samples.util.LauncherUtils.getPort;

public class EmbeddedServer {

    public static void main(String[] args) throws Exception {
        final int port = getPort(args);
        final String address = getHostAddress();

        final Vertx vertx = Vertx.vertx();
        vertx.createHttpServer().requestHandler(request -> {
            request.response().end("Hello from " + Thread.currentThread().getName());
        }).listen(port, ar -> {
            if (ar.succeeded()) {
                System.out.println("HTTP server started at http://" + address + ":" + port);
            } else {
                System.err.println("Could not start HTTP server: " + ar.cause().getMessage());
            }
        });
    }

}
