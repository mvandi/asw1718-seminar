package it.unibo.asw.vertx_samples.ex04.domain;

import io.vertx.core.json.JsonObject;
import it.unibo.asw.vertx_samples.util.JsonHelper;

import java.util.Date;
import java.util.Objects;

public class Message {

    private String content;
    private Date timestamp;

    public static Message fromJson(JsonObject jsonObject) {
        final Message message = new Message();
        JsonHelper.ifPresentString(jsonObject, "content", message::setContent);
        JsonHelper.ifPresentString(jsonObject, "timestamp", timestamp -> message.setTimestamp(new Date(timestamp)));
        return message;
    }

    private Message() {
    }

    public String getContent() {
        return content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    private void setContent(String content) {
        this.content = content;
    }

    private void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return Objects.equals(getContent(), message.getContent()) &&
                Objects.equals(getTimestamp(), message.getTimestamp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getContent(), getTimestamp());
    }
    public JsonObject toJson() {
        return new JsonObject()
                .put("content", content)
                .put("timestamp", timestamp.toString());
    }

}
