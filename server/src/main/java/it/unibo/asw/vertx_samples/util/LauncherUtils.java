package it.unibo.asw.vertx_samples.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class LauncherUtils {

    public static String getHostAddress() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public static int getPort(String[] args) {
        return args.length == 0 ? 8080 : Integer.parseInt(args[0]);
    }

    private LauncherUtils() {
    }

}
