package it.unibo.asw.vertx_samples.ex04;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.StaticHandler;
import it.unibo.asw.vertx_samples.ex04.domain.Message;
import it.unibo.asw.vertx_samples.ex04.repositories.MessageRepository;

import java.util.stream.Collectors;

import static it.unibo.asw.vertx_samples.util.LauncherUtils.getHostAddress;

public class ChatServer extends AbstractVerticle {

    private EventBus eventBus;

    private final MessageRepository repository;

    public ChatServer() {
        repository = new MessageRepository();
    }

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        eventBus = vertx.eventBus();
    }

    @Override
    public void start() throws Exception {
        final Router router = Router.router(vertx);

        router.route("/api/messages/events/*")
                .handler(sockJSHandler());
        router.route()
                .handler(BodyHandler.create());

        router.route().handler(StaticHandler.create("chatApp"));

        eventBus.<JsonObject>consumer(C.chat.to.server.message, m -> onMessage(m.body()));
        eventBus.<JsonObject>consumer(C.chat.to.server.messages, m -> onMessages());

        final int port = config().getInteger("httpPort");
        final String address = getHostAddress();
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(port, ar -> {
                    if (ar.succeeded()) {
                        System.out.println("HTTP server started at http://" + address + ":" + port);
                    } else {
                        System.err.println("Could not start HTTP server: " + ar.cause().getMessage());
                    }
                });
    }

    private void onMessages() {
        System.out.println("Sending message list to new client");
        eventBus.publish(C.chat.to.client.messages,
                new JsonArray(repository.findAll().stream()
                        .map(Message::toJson)
                        .collect(Collectors.toList())));
    }

    private void onMessage(JsonObject json) {
        final Message message = Message.fromJson(json);
        System.out.println("Sending new message to clients");
        repository.save(message);
        eventBus.publish(C.chat.to.client.message, message.toJson());
    }

    private SockJSHandler sockJSHandler() {
        final BridgeOptions options = new BridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddress(C.chat.to.client.messages))
                .addInboundPermitted(new PermittedOptions().setAddress(C.chat.to.server.messages))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.chat.to.client.message))
                .addInboundPermitted(new PermittedOptions().setAddress(C.chat.to.server.message));

        return SockJSHandler.create(vertx).bridge(options);
    }

}
