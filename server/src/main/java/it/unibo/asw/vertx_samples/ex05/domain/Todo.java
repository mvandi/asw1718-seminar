package it.unibo.asw.vertx_samples.ex05.domain;

import it.unibo.asw.vertx_samples.util.JsonHelper;
import it.unibo.asw.vertx_samples.util.ObjectIdHelper;
import io.vertx.core.json.JsonObject;
import org.bson.types.ObjectId;

import java.util.Objects;

public class Todo {

    private ObjectId id;
    private String title;
    private boolean done;

    public static Todo fromJson(JsonObject json) {
        final Todo todo = new Todo();

        ObjectIdHelper.fromJson(json).ifPresent(todo::setId);

        JsonHelper.ifPresentString(json, "title", todo::setTitle);

        JsonHelper.ifPresentBoolean(json, "done", todo::setDone);

        return todo;
    }

    public Todo(final String title, final boolean done) {
        setTitle(title);
        setDone(done);
    }

    public Todo(final String title) {
        this(title, false);
    }

    private Todo() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = Objects.requireNonNull(title);
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Todo)) return false;
        final Todo that = (Todo) o;
        return Objects.equals(this.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public JsonObject toJson() {
        final JsonObject json = new JsonObject();

        if (id != null) {
            json.mergeIn(ObjectIdHelper.toJson(id));
        }

        if (title != null) {
            json.put("title", getTitle());
        }

        return json.put("done", isDone());
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}
