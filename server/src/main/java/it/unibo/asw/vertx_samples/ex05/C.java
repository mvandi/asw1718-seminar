package it.unibo.asw.vertx_samples.ex05;

final class C {

    static final class todos {
        static final String stored = "todos.stored";

        static final String destroyed = "todos.destroyed";

        static final String updated = "todos.updated";

        private todos() {
        }
    }

    private C() {
    }

}
