package it.unibo.asw.vertx_samples.ex04;

final class C {

    static final class chat {
        static final class to {
            static final class server {
                private static final String prefix = C.prefix + ".server";
                static final String message = prefix + ".message";
                static final String messages = prefix + ".messages";
            }

            static final class client {
                private static final String prefix = C.prefix + ".client";
                static final String message = prefix + ".message";
                static final String messages = prefix + ".messages";
            }
        }
    }

    private static final String prefix = "chat.to";

}
