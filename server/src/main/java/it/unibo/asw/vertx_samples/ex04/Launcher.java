package it.unibo.asw.vertx_samples.ex04;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

import static it.unibo.asw.vertx_samples.util.LauncherUtils.getPort;

public class Launcher {

    public static void main(String[] args) {
        final int port = getPort(args);
        final JsonObject config = new JsonObject().put("httpPort", port);

        final DeploymentOptions options = new DeploymentOptions().setConfig(config);

        Vertx.clusteredVertx(new VertxOptions(), ar -> {
            if (ar.succeeded()) {
                final Vertx vertx = ar.result();
                vertx.deployVerticle(ChatServer::new, options);
            } else {
                System.err.println("Could not start server verticle: " + ar.cause().getMessage());
            }
        });
    }

}
