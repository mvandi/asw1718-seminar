package it.unibo.asw.vertx_samples.ex05.repositories;

import io.reactivex.Observable;
import it.unibo.asw.vertx_samples.ex05.domain.Todo;
import it.unibo.asw.vertx_samples.util.ObjectIdHelper;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import org.bson.types.ObjectId;

public class MongoTodoRepository implements TodoRepository {

    private static final String COLLECTION_NAME = "todos";
    private final MongoClient client;

    public MongoTodoRepository(MongoClient client) {
        this.client = client;
    }

    public Observable<Todo> findAll() {
        return client.rxFind(COLLECTION_NAME, new JsonObject())
                .toObservable()
                .flatMapIterable(x -> x)
                .map(Todo::fromJson);
    }

    public Single<Todo> findById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOne(COLLECTION_NAME, query, null)
                .map(Todo::fromJson);
    }

    public Single<ObjectId> save(Todo todo) {
        final ObjectId id = todo.getId();

        return client.rxSave(COLLECTION_NAME, todo.toJson())
                .map(newId -> id == null
                        ? ObjectIdHelper.fromString(newId)
                        : id);
    }

    public Completable deleteById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOneAndDelete(COLLECTION_NAME, query)
                .toCompletable();
    }

}
