package it.unibo.asw.vertx_samples.ex04.repositories;

import it.unibo.asw.vertx_samples.ex04.domain.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageRepository {

    private final List<Message> messages;

    public MessageRepository() {
        this.messages = new ArrayList<>();
    }

    public List<Message> findAll() {
        return messages;
    }

    public void save(Message message) {
        messages.add(message);
    }

}
