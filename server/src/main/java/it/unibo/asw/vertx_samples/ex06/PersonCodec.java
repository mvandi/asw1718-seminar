package it.unibo.asw.vertx_samples.ex06;

import io.vertx.core.json.JsonObject;

public class PersonCodec extends AbstractMessageCodec<Person, Person> {

    @Override
    protected void encodeToWire(JsonObject jsonObject, Person person) {
        jsonObject.put("firstName", person.getFirstName());
        jsonObject.put("lastName", person.getLastName());
        jsonObject.put("age", person.getAge());
    }

    @Override
    protected Person decodeFromWire(JsonObject jsonObject) {
        final String firstName = jsonObject.getString("firstName");
        final String lastName = jsonObject.getString("lastName");
        final int age = jsonObject.getInteger("age");
        return new Person(firstName, lastName, age);
    }

    @Override
    public Person transform(Person person) {
        return person;
    }

    @Override
    public String name() {
        return "person";
    }

}
