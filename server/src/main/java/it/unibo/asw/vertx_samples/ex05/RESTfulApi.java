package it.unibo.asw.vertx_samples.ex05;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.vertx.core.Context;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.mongo.MongoClient;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.StaticHandler;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler;
import it.unibo.asw.vertx_samples.ex05.domain.Todo;
import it.unibo.asw.vertx_samples.ex05.repositories.MongoTodoRepository;
import it.unibo.asw.vertx_samples.ex05.repositories.TodoRepository;
import it.unibo.asw.vertx_samples.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.function.Function;

import static io.vertx.core.http.HttpMethod.PATCH;
import static io.vertx.core.http.HttpMethod.PUT;
import static it.unibo.asw.vertx_samples.util.LauncherUtils.getHostAddress;

public class RESTfulApi extends AbstractVerticle {

    private final CompositeDisposable disposables = new CompositeDisposable();
    private TodoRepository repository;
    private EventBus eventBus;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        final MongoClient client = MongoClient.createNonShared(this.vertx, config());
        repository = new MongoTodoRepository(client);
        eventBus = vertx.eventBus();
    }

    @Override
    public void start() throws Exception {
        final Router apiRouter = Router.router(vertx);

        apiRouter.route("/todos/events/*")
                .handler(sockJSHandler());

        apiRouter.route()
                .handler(BodyHandler.create());

        apiRouter.get("/todos")
                .produces("application/json")
                .handler(wrap(this::index));

        apiRouter.post("/todos")
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(this::store));

        apiRouter.get("/todos/:id")
                .produces("application/json")
                .handler(wrap(this::show));

        apiRouter.route("/todos/:id")
                .method(PUT)
                .method(PATCH)
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(this::update));

        apiRouter.delete("/todos/:id")
                .produces("application/json")
                .handler(wrap(this::destroy));

        final Router mainRouter = Router.router(vertx);

        mainRouter.mountSubRouter("/api", apiRouter);
        mainRouter.route().handler(StaticHandler.create("todoApp"));

        final int port = config().getInteger("httpPort");
        final String address = getHostAddress();
        vertx.createHttpServer()
                .requestHandler(mainRouter::accept)
                .listen(port, ar -> {
                    if (ar.succeeded()) {
                        System.out.println("HTTP server started at http://" + address + ":" + port);
                    } else {
                        System.err.println("Could not start HTTP server: " + ar.cause().getMessage());
                    }
                });
    }

    @Override
    public void stop() {
        disposables.clear();
        disposables.dispose();
    }

    private Disposable index(RoutingContext ctx) {
        return repository.findAll()
                .map(Todo::toJson)
                .toList()
                .map(JsonArray::new)
                .map(Object::toString)
                .subscribe(json -> ctx.response().end(json));
    }

    private Disposable store(RoutingContext ctx) {
        final JsonObject body = ctx.getBodyAsJson();
        final Todo todo = Todo.fromJson(body);

        return repository.save(todo)
                .map(ObjectIdHelper::toJson)
                .map(todo.toJson()::mergeIn)
                .subscribe(json -> {
                    eventBus.publish(C.todos.stored, json);
                    ctx.response().end();
                });
    }

    private Disposable show(RoutingContext ctx) {
        final String id = ctx.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return repository.findById(oid)
                .map(Todo::toJson)
                .map(Object::toString)
                .subscribe(json -> ctx.response().end(json));
    }

    private Disposable update(RoutingContext ctx) {
        final String id = ctx.request().getParam("id");
        final JsonObject body = ctx.getBodyAsJson().put("_id", id);
        final Todo todo = Todo.fromJson(body);

        return repository.save(todo)
                .map(oid -> todo.toJson())
                .subscribe(json -> {
                    eventBus.publish(C.todos.updated, json);
                    ctx.response().end();
                });
    }

    private Disposable destroy(RoutingContext ctx) {
        final String id = ctx.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return repository.deleteById(oid)
                .subscribe(() -> {
                    final JsonObject json = new JsonObject().put("_id", id);
                    eventBus.publish(C.todos.destroyed, json);
                    ctx.response().end();
                });
    }

    private Handler<RoutingContext> wrap(Function<? super RoutingContext, ? extends Disposable> handler) {
        return ctx -> disposables.add(handler.apply(ctx));
    }

    private SockJSHandler sockJSHandler() {
        final BridgeOptions options = new BridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddress(C.todos.stored))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.todos.destroyed))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.todos.updated));

        return SockJSHandler.create(vertx).bridge(options);
    }

}
