package it.unibo.asw.vertx_samples.ex05.repositories;

import io.reactivex.Observable;
import it.unibo.asw.vertx_samples.ex05.domain.Todo;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.bson.types.ObjectId;

public interface TodoRepository {

    Observable<Todo> findAll();

    Single<Todo> findById(ObjectId id);

    Single<ObjectId> save(Todo todo);

    Completable deleteById(ObjectId id);

}
