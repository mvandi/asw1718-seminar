package it.unibo.asw.vertx_samples.ex03;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;

import static it.unibo.asw.vertx_samples.util.LauncherUtils.getHostAddress;
import static it.unibo.asw.vertx_samples.util.LauncherUtils.getPort;

public class ServerVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        final int port = config().getInteger("httpPort");
        final String address = getHostAddress();

        final Router router = Router.router(vertx);

        router.get("/")
                .produces("text/plain")
                .handler(ctx -> ctx.response().end("Hello from " + Thread.currentThread().getName()));

        vertx.createHttpServer().requestHandler(router::accept).listen(port, ar -> {
            if (ar.succeeded()) {
                System.out.println("HTTP server started at http://" + address + ":" + ar.result().actualPort());
            } else {
                System.err.println("Could not start HTTP server: " + ar.cause().getMessage());
            }
        });
    }

    public static void main(String[] args) {
        final int port = getPort(args);
        final Vertx vertx = Vertx.vertx();
        final JsonObject config = new JsonObject().put("httpPort", port);
        vertx.deployVerticle(ServerVerticle::new, new DeploymentOptions().setConfig(config));
    }

}
