#!/bin/bash
if [ "$#" != "1" ]; then
    echo "Usage: $0 <http_port>"
    exit 1
fi
java -cp build/libs/server-1.0.jar it.unibo.asw.vertx_samples.ex04.Launcher $1
